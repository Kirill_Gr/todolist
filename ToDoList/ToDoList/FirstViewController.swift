//
//  FirstViewController.swift
//  ToDoList
//
//  Created by Nik on 03/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

var itemsData = [String]()

class FirstViewController: UIViewController, UITableViewDelegate {

    @IBOutlet var itemsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if NSUserDefaults.standardUserDefaults().objectForKey("itemsData") != nil {
            itemsData = NSUserDefaults.standardUserDefaults().objectForKey("itemsData") as! [String]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        itemsTable.reloadData()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return itemsData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        cell.textLabel?.text = itemsData[indexPath.row]
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            itemsData.removeAtIndex(indexPath.row)
            NSUserDefaults.standardUserDefaults().setObject(itemsData, forKey: "itemsData")
            itemsTable.reloadData()
        }
    }
}

