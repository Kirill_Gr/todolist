//
//  SecondViewController.swift
//  ToDoList
//
//  Created by Nik on 03/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet var item: UITextField!
    
    @IBAction func addItem(sender: AnyObject) {
        if (item.text != nil) {
            itemsData.append(item.text!)
            NSUserDefaults.standardUserDefaults().setObject(itemsData, forKey: "itemsData")
            item.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn (textField: UITextField!) -> Bool {
        item.resignFirstResponder()
        return true
    }
    
}

